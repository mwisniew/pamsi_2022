#include "Packet.hh"

std::istream & operator >> (std::istream & strm, Packet & packet){
  std::string tmp;
  double rate;
  bool exception_caught = false;
  char bin;

  //get index
  getline(strm, tmp, ',');
  if(strm.good())
    packet.set_index(std::stoi(tmp));
  else return strm;

  //get title in format << "title", >> or << title, >>
  if(strm.peek() == '"'){
    strm >> bin;
    getline(strm, tmp, '"');
    packet.set_name(tmp);
    strm >> bin;
  } else {
    getline(strm, tmp, ',');
    packet.set_name(tmp);
  }

  //get rating
  getline(strm, tmp);
  if(strm.good()){
    try{
      rate = std::stod(tmp);
    } //if tmp is not a number set rating as -1.0
    catch(const std::invalid_argument & e){
      packet.set_rating(-1.0);
      exception_caught = true;
    }
  }else return strm;

  if(!exception_caught)
    packet.set_rating(rate);

  return strm;

}


std::ostream & operator << (std::ostream & strm, const Packet & packet){
  strm << packet.get_index() << ',' << packet.get_name() << ',' << packet.get_rating();
  return strm;
}
