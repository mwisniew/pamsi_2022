#include <iostream>
#include <sstream>
#include <string>
#include <chrono>
#include "Packet.hh"
#include "Pair.hh"
#include "List.hh"
#include "MsgGen.hh"
#include "MsgRec.hh"

using MSQ = DLList<Pair<DataPacket<std::string>, int>>;
using time_point = std::chrono::time_point<std::chrono::steady_clock>;

int main(){
  std::stringbuf buf;
  std::iostream dataStream(&buf);

  std::string inMsg;
  std::string outMsg;

  DataPacket<std::string> currentPacket;

  MSQ dataQueue;

  size_t packetsN;
  MsgGen sender;

  std::cout << "Message:";

  getline(std::cin, inMsg);

  std::cout << "Number of packets:";
  
  std::cin >> packetsN;



  
  sender = MsgGen(inMsg, packetsN);
    
  dataStream << sender;
  
  std::cout << "Packets: " << sender << std::endl;


  for(size_t i = 0; i < packetsN && dataStream.good(); i++){
    dataStream >> currentPacket;

    dataQueue.push_back(Pair<DataPacket<std::string>, int>(currentPacket, currentPacket.getIndex()));
  }

  time_point start = std::chrono::steady_clock::now();

  MsgRec rec(dataQueue);
  
  time_point end = std::chrono::steady_clock::now();
  
  dataQueue.clear();
  
  std::chrono::duration<double> diff = end - start;
  
  std::cout << "Received : " << rec.getMsg() << std::endl;
  
  std::cout << "Packets | time" << std::endl;

  std::cout << packetsN << "   " << diff.count() << std::endl;
    
  return 0;
}
