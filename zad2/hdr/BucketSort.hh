#ifndef BUCKETSORT_HH
#define BUCKETSORT_HH

#include "MergeSort.hh"
#include <iostream>

template<typename T>
class BucketSort{
  public:
    /*! \brief Constructor
     *
     * \param unsorted - pointer to unsorted array
     * \param size - number of elements to sort
     *
     */
    BucketSort(T* unsorted, size_t size);
    
    /*! \brief Constructor
     *
     * \param unsorted - copy of unsorted array
     *
     */
    BucketSort(Array<T> unsorted);
    
    /*! \brief Cast operator
     *
     * \return - pointer to sorted array
     *
     */
    operator T*() { passed = true;  return sorted; } 

  private:
    T* sorted;
    bool passed;

    T max(T* arr, size_t size);
    T min(T* arr, size_t size);
};

template<typename T>
BucketSort<T>::BucketSort(T* unsorted, size_t size){

  T max_element = max(unsorted, size);
  T min_element = min(unsorted, size);

  //allocate memory for 10 arrays
  Array<T>* buckets = new Array<T>[10];
  
  for(int i = 0; i < 10; i++)
    buckets[i] = Array<T>(new T[size], size);
  
  size_t bucket_sizes[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
  
  //calculate step as (max-min)/buckets_amount
  double step = (static_cast<double>(max_element) - static_cast<double>(min_element)) / 10;

  bool placed = false;
  
  //split data into buckets
  for(size_t i = 0; i < size; i++){
    for(int j = 0; j < 10 && !placed ; j++){
      //if data is lesser than (n+1) * step + min, put that data in n-th bucket
      if(unsorted[i] <= ((j + 1) * step + min_element)){
        buckets[j][bucket_sizes[j]++] = unsorted[i];
        placed = true;
      }
    }
    placed = false;
  }

  for(int i = 0; i < 10; i++){
    buckets[i].shrink(bucket_sizes[i]);
  }
  
  sorted = new T[size];
  T* sorted_bucket;
  
  //sort the buckets using merge sort
  for(int i = 0, k = 0; i < 10; i++){
    sorted_bucket = MergeSort(buckets[i]);
    for(size_t j = 0; j < bucket_sizes[i]; j++)
      sorted[k++] = sorted_bucket[j];
  }
}

template<typename T>
BucketSort<T>::BucketSort(Array<T> unsorted){
  sorted = BucketSort(unsorted.get_array(), unsorted.get_size());
  passed = false;
}

template<typename T>
T BucketSort<T>::max(T* arr, size_t size){
  T max = arr[0];
  
  for(size_t i = 0; i < size; i++){
    if(arr[i] > max)
      max = arr[i];
  }
  
  return max;
}

template<typename T>
T BucketSort<T>::min(T* arr, size_t size){
  T min = arr[0];
  
  for(size_t i = 0; i < size; i++){
    if(arr[i] < min)
      min = arr[i];
  }
  
  return min;
}

#endif // BUCKETSORT_HH
