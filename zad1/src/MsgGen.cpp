#include "MsgGen.hh"

MsgGen::MsgGen(const std::string & msg, size_t n){
  if(n <= msg.size()){
    fullMsg = msg;
    packets = new DataPacket<std::string>[n];
    size = n;
  
    size_t maxPacketSize = ((msg.size() - msg.size() % n) / n) + 1;
    DataPacket<std::string>* tmpTab = new DataPacket<std::string>[n];
    DataPacket<std::string> toSwap;
    std::string tmp;
    char buf;
    
    std::random_device rand_dev;
    static std::mt19937 engine(rand_dev());
    std::uniform_real_distribution<> randIndex(0, n);
    int currIndex;
    
    std::stringstream splitStream(msg);
    
    for(size_t i = 0; i < n; i++){
      for(size_t j = 0; j < maxPacketSize; j++){
        splitStream.get(buf);
        if(splitStream.good()){
          if(buf != 0)
            tmp.push_back(buf);
          else
            tmp.push_back(' ');
        }
          
      }
      #ifdef DEBUG
      std::cerr << tmp << ' ' << maxPacketSize << ' ' << i << std::endl;
      #endif
      tmpTab[i] = DataPacket<std::string>(i, tmp);
      tmp.erase();
    }
    
    for(size_t i = n - 1; i > 0; i--){
      randIndex = std::uniform_real_distribution<>(0, i);
      currIndex = randIndex(engine);
      
      toSwap = tmpTab[currIndex];
      tmpTab[currIndex] = tmpTab[i];
      tmpTab[i] = toSwap;
    }
    
    for(size_t i = 0; i < n; i++){
      packets[i] = tmpTab[i];
    }
    
    delete[] tmpTab;
  } else {
    std::cerr << "error" << std::endl;
    exit(-1);
  }

}

MsgGen::MsgGen(const MsgGen & other){
  if(packets != nullptr){
    delete[] packets;
  }
  fullMsg = other.fullMsg;
  size = other.size;
  
  packets = new DataPacket<std::string>[size];
  
  for(size_t i = 0; i < size; i++){
    packets[i] = other.packets[i];
    #ifdef DEBUG
    std::cerr << packets[i] << std::endl;
    #endif
  }
}

MsgGen & MsgGen::operator= (const MsgGen & other){
  if(packets != nullptr){
    #ifdef DEBUG
    std::cerr << "deleting old packet" << std::endl;
    #endif
  
    delete[] packets;
    
    #ifdef DEBUG
    std::cerr << "deleted old packet" << std::endl;
    #endif
  }
  fullMsg = other.fullMsg;
  size = other.size;
  
  packets = new DataPacket<std::string>[size];
  
  for(size_t i = 0; i < size; i++){
    packets[i] = other.packets[i];
    #ifdef DEBUG
    std::cerr << packets[i] << std::endl;
    #endif
  }
  return *this;
}

std::ostream & operator << (std::ostream & strm, const MsgGen & msg){
  for(size_t i = 0; i < msg.getSize(); i++){
    strm << msg[i];
  }
  return strm;
}
