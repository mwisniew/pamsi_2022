#ifndef MERGESORT_HH  
#define MERGESORT_HH

template<typename T>
class MergeSort{
private:
  T* sorted;
  bool passed;
  
  Array<T> split_merge(Array<T> toSplit);
  Array<T> merge(Array<T> larr, Array<T> rarr);
public:
  
  /*! \brief Constructor
   *
   * \param unsorted - pointer to unsorted array
   * \param size - number of elements to sort
   *
   */
  MergeSort(T* unsorted, size_t size);
  
  /*! \brief Constructor
   *
   * \param unsorted - copy of unsorted array
   *
   */
  MergeSort(Array<T> unsorted);
  
  /*! \brief Cast operator
   *
   * \return - pointer to sorted array
   *
   */
  operator T*() { passed = true;  return sorted; } 

};


template<typename T>
MergeSort<T>::MergeSort(T* unsorted, size_t size){
  Array<T> toSort(unsorted, size);
  
  Array<T> sortedArr = split_merge(toSort);
  
  sorted = new T[sortedArr.get_size()];
  for(size_t i = 0; i < sortedArr.get_size(); i++)
    sorted[i] = sortedArr[i];
  
  passed = false;
}

template<typename T>
MergeSort<T>::MergeSort(Array<T> unsorted){
  
  Array<T> sortedArr = split_merge(unsorted);
  
  sorted = new T[sortedArr.get_size()];
  for(size_t i = 0; i < sortedArr.get_size(); i++)
    sorted[i] = sortedArr[i];
  
  passed = false;
}

template<typename T>
Array<T> MergeSort<T>::merge(Array<T> larr, Array<T> rarr){
  Array<T> merged(new T[larr.get_size() + rarr.get_size()], larr.get_size() + rarr.get_size());
  
  size_t j = 0;
  size_t k = 0;
  
  for(size_t i = 0; i < merged.get_size(); i++){
    //if left array is fully copied, copy right array
    if(j == larr.get_size() && k < rarr.get_size()){
      merged[i] = rarr[k++];
    }
    //if right array is fully copied, copy left array
    if(k == rarr.get_size() && j < larr.get_size()){
      merged[i] = larr[j++];
    }
    //if arrays are not fully copied
    if(k != rarr.get_size() && j != larr.get_size()){
      if(larr[j] < rarr[k]){  
        merged[i] = larr[j++];
      } else {
        merged[i] = rarr[k++];
      }
    
    }
  }
  
  return merged;
}

template<typename T>
Array<T> MergeSort<T>::split_merge(Array<T> toSplit){
  if(toSplit.get_size() == 1 || toSplit.get_size() == 0)
    return toSplit;
    
  size_t middle = toSplit.get_size() / 2;
  
  Array<T> larr(new T[middle], middle);
  Array<T> rarr(new T[toSplit.get_size() - middle], toSplit.get_size() - middle);
  
  //split array to two parts
  for(size_t i = 0; i < larr.get_size(); i++)
    larr[i] = toSplit[i];
    
  for(size_t i = 0; i < rarr.get_size(); i++)
    rarr[i] = toSplit[i + middle];
  
  //merge recursively split arrays  
  return merge(split_merge(larr), split_merge(rarr));
}
#endif
