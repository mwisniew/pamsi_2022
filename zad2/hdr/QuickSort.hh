#ifndef QUICKSORT_HH
#define QUICKSORT_HH

#include "Array.hh"
#include <iostream>

template<typename T>
class QuickSort{
  public:
    /*! \brief Constructor
     *
     * \param unsorted - pointer to unsorted array
     * \param size - number of elements to sort
     *
     */
    QuickSort(T* unsorted, size_t size);

    /*! \brief Constructor
     *
     * \param unsorted - copy of unsorted array
     *
     */
    QuickSort(Array<T> unsorted);

    /*! \brief Cast operator
     *
     * \return - pointer to sorted array
     *
     */
    operator T*() { passed = true;  return sorted; }

  private:
    T* sorted;
    bool passed;

    Array<T> quicksort_step(const Array<T> & unsorted);
    T median_of_three(T a, T b, T c);
};

template<typename T>
QuickSort<T>::QuickSort(T* unsorted, size_t size){

  Array<T> uarr = Array<T>(unsorted, size);
  Array<T> sarr = quicksort_step(uarr);

  //make a copy of array
  sorted = new T[sarr.get_size()];
  for(size_t i = 0; i < sarr.get_size(); i++)
    sorted[i] = sarr[i];

  passed = false;
}

template<typename T>
QuickSort<T>::QuickSort(Array<T> unsorted){
  Array<T> sarr = quicksort_step(unsorted);

  sorted = new T[sarr.get_size()];
  for(size_t i = 0; i < sarr.get_size(); i++)
    sorted[i] = sarr[i];

  passed = false;

}

template<typename T>
Array<T> QuickSort<T>::quicksort_step(const Array<T> & unsorted){

  if (unsorted.get_size() == 1) return unsorted;
  if (unsorted.get_size() == 0) return unsorted;

  //pivot as median of first, last and middle element
  T pivot = median_of_three(unsorted[0], unsorted[unsorted.get_size() / 2], unsorted[unsorted.get_size() - 1]);

  bool pivot_found = false;
  bool g_arr_sorted = true;
  Array<T> ltp(new T[unsorted.get_size()], unsorted.get_size());
  Array<T> gtp(new T[unsorted.get_size()], unsorted.get_size());


  size_t l_size = 0;
  size_t g_size = 0;

  //split unsorted elements to arrays <pivot and >=pivot
  for(size_t i = 0; i < unsorted.get_size(); i++){
    if(unsorted[i] < pivot){
      ltp[l_size++] = unsorted[i];
    } else if(unsorted[i] > pivot){
      gtp[g_size++] = unsorted[i];
    } else {
      if(!pivot_found && unsorted[i] == pivot){
        pivot_found = true;
      } else gtp[g_size++] = unsorted[i];
      }
    }

    ltp.shrink(l_size);
    gtp.shrink(g_size);

    //Check if all elements are equal
    if(l_size == 0){
      for(size_t i = 0; i < g_size && g_arr_sorted; i++){
        g_arr_sorted = (gtp[i] <= pivot);
      }

      if(g_arr_sorted)
        return unsorted;
    }

  //merge sorted arrays
  Array<T> tmp = (quicksort_step(ltp) + pivot + quicksort_step(gtp));

  return tmp;

}

template<typename T>
T QuickSort<T>::median_of_three(T a, T b, T c){


  if((b >= a && b <= c) || (b >= c && b <= a))
    return b;
  else if ((c >= a && c <= b) || (c >= b && c <= a))
    return c;
    else return a;
}

#endif // QUICKSORT_HH
