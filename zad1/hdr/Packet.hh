#ifndef PACKET_HH
#define PACKET_HH

#include <iostream>
#include <string>

#include "SmartPointer.hh"

  /*
  concept StreamFriendly - requires that typename T for DataPacket<T> has defined
  istream >> T and ostream << T operators. If not, causes compilation error with 
  clear message.
  */

template<typename T>
concept StreamFriendly = requires(T x){
  { std::cin >> x };
  { std::cout << x };
};


template<StreamFriendly T>
class DataPacket{
private:
  int packet_index;
  SharedPointer<T> data;
public:

  /*!
   *  \brief Constructor of data packet in format [<index>:<data>]
   *  \param index - index of packet
   *  \param _data - data of packet
   */

  DataPacket(const int & index, const T & _data) : packet_index(index), data(new T(_data)) {}
  
  /*!
   *  \brief Constructor of empty data packet
   */
  
  DataPacket() {}
  
  /*!
   *  \brief Get packet index
   *  \return - packet index
   */
  
  const int & getIndex() const {return packet_index;}
  
  /*!
   *  \brief Get packet data
   *  \return - packet data
   */
  
  const T & getData() const {return *data;}
  
  /*!
   *  \brief Set packet index
   *  \param index - new index of packet
   */
  
  void setIndex(const int & index) { packet_index = index; }
  
  /*!
   *  \brief Set packet data
   *  \param _data - new data of packet
   */
  
  void setData(const T & _data) { data = SharedPointer<T>(new T(_data)); }
};

template<StreamFriendly T1> 
std::istream & operator >> (std::istream & strm, DataPacket<T1> & packet){
  char buffer;
  T1 data;
  int index;
  
  strm >> buffer;
  if(buffer != '['){
    strm.setstate(std::ios::failbit);
    return strm;
  }
  
  strm >> index;
  
  packet.setIndex(index);
  
  strm >> buffer;
  if(buffer != ':'){
    strm.setstate(std::ios::failbit);
    return strm;
  }
  
  strm >> data;
  
  packet.setData(data);
  
  strm >> buffer;
  if(buffer != ']'){
    strm.setstate(std::ios::failbit);
    return strm;
  }
  
  return strm;
}

template<StreamFriendly T1> 
std::ostream & operator << (std::ostream & strm, const DataPacket<T1> & packet){
  strm << '[' << packet.getIndex() << ':' << packet.getData() << ']';
  return strm;
}

#endif
