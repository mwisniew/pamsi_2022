#ifndef PRIORITYQUEUE_HH
#define PRIORITYQUEUE_HH

#include <concepts>

/*
concept Comparable - requires that KEY in priority queue template has > and < operators 
and that return type can be converted to bool
*/
template<typename T>
concept Comparable = requires(T x) {
  { x < x } -> std::convertible_to<bool>;
  { x > x } -> std::convertible_to<bool>;
};


template<typename DATA, Comparable KEY>
class PriorityQueue{
private:
  DLList<Pair<DATA, KEY>> container;
public:
  
  /*!
   *  \brief Constructor of priority queue implemented on sorted list
   *  \param sequence - list with unsorted data
   */

  PriorityQueue(const DLList<Pair<DATA, KEY>> & sequence);
  
  /*!
   *  \brief Constructor of empty priority queue
   */
  
  PriorityQueue() {}
  
  /*!
   *  \brief Remove first element
   */
  
  void pop_front() {container.pop_front();}
  
  /*!
   *  \brief Get first element
   *  \return - data from first element
   */
  
  const Pair<DATA, KEY> & front() {return container.front();}
  
  /*!
   *  \brief Insert new element with given priority 
   *  \param data - new element data
   *  \param priority - priority of new element
   */
  
  void insert(const DATA & data, const KEY & priority){}
  
  /*!
   *  \brief Get length of queue
   *  \return - number of queue elements
   */
  
  const int & getSize() const {return container.getSize();}
  
};

template<typename DATA, Comparable KEY>
PriorityQueue<DATA, KEY>::PriorityQueue(const DLList<Pair<DATA, KEY>> & base_sequence){
  if(base_sequence.getSize() != 0){
    DLList<Pair<DATA, KEY>> sequence = base_sequence;
    Pair<DATA,KEY> tmp = sequence.front();
    int min_index;
    
    while(sequence.getSize() != 0){
      tmp = sequence.front();
      min_index = 0;
      for(int i = 0;  i < sequence.getSize(); i++){
        if(tmp.getSecond() > sequence[i].getSecond()){
          tmp = sequence[i];
          min_index = i;    
        }
      }
      
      container.push_back(tmp); 
      sequence.remove(min_index);
    } 
  }
};

#endif
