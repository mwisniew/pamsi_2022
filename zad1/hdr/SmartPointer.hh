#ifndef POINTERFRIENDLY_CONCEPT
#define POINTERFRIENDLY_CONCEPT

#include <type_traits>

/*
concept PointerFriendly - doesn't allow using smart pointer with types T* and T[] due to 
missing destructors for those types. Using delete on type T[] has undefined behavior same
as creating specialisation using std::is_array_v<T>
*/

template<typename T>
concept PointerFriendly = !(std::is_pointer_v<T> or std::is_array_v<T>);
#endif

#ifndef SMARTPOINTER_HH
#define SMARTPOINTER_HH

#include <cstddef>

#ifdef DEBUG
#include <iostream>
#endif

template<PointerFriendly T>
class SharedPointer{
private:
  class RefCounter;
  
  RefCounter* use_counter = nullptr;
  T* data_pointer = nullptr;
public:

  /*!
   *  \brief Constructor of empty shared pointer
   */

  SharedPointer() : use_counter(nullptr), data_pointer(nullptr) {}
  
  /*!
   *  \brief Constructor of shared pointer
   *  \param dataptr - pointer to allocated memory
   */
  
  SharedPointer(T* dataptr) {
    data_pointer = dataptr;
    
    #ifdef DEBUG
    std::cerr << "param: " << data_pointer << std::endl;
    #endif
    
    if(dataptr != nullptr){
      use_counter = new RefCounter;
      use_counter->addRef();
    }
    else use_counter = nullptr;
  }
  
  /*!
   *  \brief Copy constructor
   *  \param other - pointer to copy from
   */  
  
  SharedPointer(const SharedPointer & other){
    #ifdef DEBUG
    std::cerr << "copy: " << other.data_pointer << std::endl;
    #endif
  
    if(data_pointer != nullptr){
      if(use_counter->decRef() == 0){
        delete data_pointer;
        delete use_counter;
      }
    }
    data_pointer = other.data_pointer;
    use_counter = other.use_counter;
    if(data_pointer != nullptr){
      use_counter->addRef();  
    }
  }
  
  /*!
   *  \brief Destructor (frees memory if it's the last pointer to data)
   */
  
  ~SharedPointer(){
    #ifdef DEBUG
    if(data_pointer != nullptr){
      std::cerr << "pointer: " << data_pointer << " use count: " << use_counter->getRef() << std::endl;
    }
    #endif
  
    if(data_pointer != nullptr && use_counter->decRef() == 0){
      #ifdef DEBUG
      std::cerr << "deleting: " << data_pointer << std::endl;
      #endif

      delete use_counter;
      delete data_pointer;
    }
  }
  
  /*!
   *  \brief Assignment operator
   *  \param other - pointer to copy from
   *  \return - pointer after assignment
   */
      
  SharedPointer & operator= (const SharedPointer & other){
    #ifdef DEBUG
    std::cerr << "operator=: " << other.data_pointer << std::endl;
    #endif
    if(data_pointer != nullptr){
      if(use_counter->decRef() == 0){
        delete data_pointer;
        delete use_counter;
      }
    }
    data_pointer = other.data_pointer;
    use_counter = other.use_counter;
    if(data_pointer != nullptr){
      use_counter->addRef();
    }
    return *this;
  }
  
  /*!
   *  \brief Indirection opeator
   *  \return - reference to data
   */
  
  T& operator* () const {return (*data_pointer);}
  
  /*!
   *  \brief Structure dereference operator
   *  \return - pointer to data
   */
  
  T* operator-> () const {return (data_pointer);}
  
  /*!
   *  \brief Equal to operator
   *  \param other - pointer to compare
   *  \return - true if pointers point to same data
   */
  
  bool operator== (const SharedPointer & other) const{
    return (data_pointer == other.data_pointer);
  }

  /*!
   *  \brief Not equal to operator
   *  \param other - pointer to compare
   *  \return - false if pointers point to same data
   */

  bool operator!= (const SharedPointer & other) const{
    return !(*this == other);
  }
  
  /*!
   *  \brief Equal to operator
   *  \param other - nullpointer
   *  \return - true if data_pointer is null
   */
  
  bool operator== (std::nullptr_t) const{
    return (data_pointer == nullptr);
  }
  
  /*!
   *  \brief Not equal to operator
   *  \param other - nullpointer
   *  \return - false if data_pointer is null
   */

  bool operator!= (std::nullptr_t) const{
    return !(*this == nullptr);
  }
  
  /*!
   *  \brief Get number of pointers with same data 
   *  \return - number of pointers pointing to the same memory
   */
  
  unsigned int use_count() const{
    return use_counter->getRef(); 
  }
  
};

template<typename T>
class SharedPointer<T>::RefCounter{
private:
  unsigned int ref_count;
public:
  
  /*!
   *  \brief Constructor of empty reference counter
   */

  RefCounter() : ref_count(0) {}
  
  /*!
   *  \brief Increase reference number
   *  \return - reference count after increase
   */
  
  unsigned int addRef() { return ++ref_count; }
  
  /*!
   *  \brief Reduce reference number
   *  \return - reference count after decrease
   */
  
  unsigned int decRef() { return --ref_count; }
  
  /*!
   *  \brief Get current reference count
   *  \return - reference number
   */
  
  unsigned int getRef() const { return ref_count; }
};

#endif
