#ifndef GAME_HH
#define GAME_HH

#include "Board.hh"
#include "Bot.hh"
#include <fstream>
#include <memory>

class Game{
private:

  std::shared_ptr<Board> game_board;
  std::shared_ptr<Bot> ai;

  pieces current_turn;


public:
  Game();

  bool is_finished() const;
  void show_board() const;

  void move_player(std::pair<size_t, size_t> location);
  void move_bot();

  size_t game_size() const {return game_board->get_size();}

  const pieces & get_turn() const {return current_turn;}
};


#endif
