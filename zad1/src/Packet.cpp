#include "Packet.hh"

template<> 
std::istream & operator >> (std::istream & strm, DataPacket<std::string> & packet){
  char buffer;
  std::string data;
  int index;
  
  strm >> buffer;
  if(buffer != '['){
    strm.setstate(std::ios::failbit);
    return strm;
  }
  
  strm >> index;
  
  packet.setIndex(index);
  
  strm >> buffer;
  if(buffer != ':'){
    strm.setstate(std::ios::failbit);
    return strm;
  }
  
  std::getline(strm, data, ']');
  
  packet.setData(data);
  
  return strm;
}

