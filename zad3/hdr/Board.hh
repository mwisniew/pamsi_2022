#ifndef BOARD_HH
#define BOARD_HH

#include <vector>
#include <iostream>

namespace edges{
  const unsigned char left_upper_corner = 218;
  const unsigned char right_upper_corner = 191;
  const unsigned char left_bottom_corner = 192;
  const unsigned char right_bottom_corner = 217;

  const unsigned char horizontal_line = 196;
  const unsigned char vertical_line = 179;

  const unsigned char top_t_junction = 194;
  const unsigned char bot_t_junction = 193;
  const unsigned char left_t_junction = 195;
  const unsigned char right_t_junction = 180;

  const unsigned char junction = 197;
}

enum pieces {p_empty = 0, p_x, p_o};

class Board{
private:
  std::vector<std::vector<pieces>> pcs;
  size_t size;
  size_t winning_streak;


  bool won_horizontal(const pieces & player) const;
  bool won_vertical(const pieces & player) const;
  bool won_diagonal(const pieces & player) const;
public:
  Board(const size_t & _size, const size_t & streak);

  const std::vector<pieces> & operator[](const size_t index) const { return pcs.at(index); }
  std::vector<pieces> & operator[](const size_t index) { return pcs.at(index); }

  std::vector<std::pair<size_t, size_t>> get_moves();

  const size_t & get_size() const { return size; }

  bool is_full();

  pieces won() const;
};

std::ostream & operator << (std::ostream & strm, const Board & brd);

#endif
