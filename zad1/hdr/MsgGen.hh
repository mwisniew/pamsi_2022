#ifndef MSGGEN_HH
#define MSGGEN_HH

#include <iostream>
#include <sstream>
#include <string>
#include "Packet.hh"
#include <random>


class MsgGen{
private:
  std::string fullMsg;
  DataPacket<std::string>* packets;
  size_t size;
public:
  
  /*!
   *  \brief Constructor of message generator 
   *  \param msg - message to cut
   *  \param n - number of packets 
   */

  MsgGen(const std::string & msg, size_t n);
  
  /*!
   *  \brief Constructor of empty message generator
   */
  
  MsgGen() {packets = nullptr; fullMsg = ""; size = 0;}
  
  /*!
   *  \brief Access operator
   *  \param index - index of packet to extract
   *  \return - packet of given index
   */
  
  const DataPacket<std::string> & operator[](const size_t & index) const { if(index < size) return packets[index]; 
                                                                           else {std::cerr << "operator [] out of bounds" << std::endl; exit(-1);} }
  
  /*!
   *  \brief Copy constructor
   *  \param other - message generator to copy
   */  
  
  MsgGen(const MsgGen & other);
  
  /*!
   *  \brief Get amount of packets 
   *  \return - number of packets
   */
  
  const size_t & getSize() const { return size; }
  
  /*!
   *  \brief Assignment operator 
   *  \param other - message generator to copy
   *  \return - generator after assignment
   */
  
  MsgGen & operator= (const MsgGen & other);
  
  /*!
   *  \brief Destructor
   */
  
  ~MsgGen() { delete[] packets; }
};

std::ostream & operator << (std::ostream & strm, const MsgGen & msg);

#endif
