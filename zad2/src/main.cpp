#include "Array.hh"
#include "QuickSort.hh"
#include "MergeSort.hh"
#include "BucketSort.hh"
#include "Packet.hh"

#include <fstream>
#include <chrono>
#include <iostream>

using time_point = std::chrono::time_point<std::chrono::steady_clock>;

int main(){
  std::ifstream data("projekt2_dane.csv");
  
  if(!data.is_open()){
    std::cerr << "File cannot be accessed!" << std::endl;
    return(-1);
  }
  
  
  
  std::string bin;
  getline(data, bin);
  
  char alg;
  Packet p;
  
  size_t mov_n;
  
  time_point start; 
  time_point stop; 
  
  Packet* sorted;
  
  
  size_t correct_mov = 0;
  
  double mean;
  double sum = 0.0;
  double median;
  
  std::cout << "Podaj ilość filmów do posortowania: ";
  std::cin >> mov_n;
  
  Array<Packet> movies(new Packet[mov_n], mov_n);
  
  for(size_t i = 0; i < mov_n; i++){
  
    data >> p;
    if(p.get_rating() != -1.0)
      movies[correct_mov++] = p;
  }
  
  movies.shrink(correct_mov);
  
  std::cout << "Liczba poprawnie wczytanych filmów: " << correct_mov << std::endl;
  std::cout << "Wybierz algorytm sortowania (q-quick sort, m-merge sort, b-bucket sort): ";
  std::cin >> alg;
  
  
  
  switch(alg){
    case 'q':
      
      start = std::chrono::steady_clock::now();
      sorted = QuickSort<Packet>(movies);
      stop = std::chrono::steady_clock::now();
    
    break;
    
    case 'm':
    
      start = std::chrono::steady_clock::now();
      sorted = MergeSort<Packet>(movies);
      stop = std::chrono::steady_clock::now();
    
    break;
    
    case 'b':
    
      start = std::chrono::steady_clock::now();
      sorted = BucketSort<Packet>(movies);
      stop = std::chrono::steady_clock::now();
    
    break;
    
    
    default:
      std::cerr << "Niepoprwany argument!" << std::endl;
      return(-2);
  
  }
  
  std::chrono::duration<double> diff = stop - start;
  
  for(size_t i = 0; i < correct_mov; i++){
    sum += sorted[i].get_rating();
  }
  
  mean = sum / correct_mov;
  
  if(correct_mov % 2 == 0){
    median = (sorted[(correct_mov / 2)] + sorted[(correct_mov / 2) + 1]) / 2;
  } else {
    median = sorted[correct_mov / 2];
  }
  
  std::cout << "Czas  |  Średnia  |  Mediana" << std::endl;
  std::cout << diff.count() << "  |  " << mean << "  |  " << median << std::endl;
  
}
