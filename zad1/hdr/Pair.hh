#ifndef PAIR_HH
#define PAIR_HH

#include "SmartPointer.hh"

template <typename T, typename U>
class Pair{
private:
  SharedPointer<T> firstType;
  SharedPointer<U> secondType;
public:

  /*!
   *  \brief Constructor of two types pair
   *  \param _firstType - data for first type
   *  \param _secondType - data for second type 
   */

  Pair(const T & _firstType, const U & _secondType) {
    firstType = SharedPointer<T>(new T(_firstType));
    secondType = SharedPointer<U>(new U(_secondType));
    }
  
  /*!
   *  \brief Copy constructor
   *  \param other - pair to copy from
   */ 
  
  Pair(const Pair<T, U> & other){
    firstType = SharedPointer<T>(new T(*(other.firstType)));
    secondType = SharedPointer<U>(new U(*(other.secondType)));
  }
  
  /*!
   *  \brief Get first type
   *  \return - data of first type
   */
  
  const T & getFirst() const { return *firstType; }
  
  /*!
   *  \brief Get second type
   *  \return - data of second type
   */
  
  const U & getSecond() const { return *secondType; }
  
  /*!
   *  \brief Set first type data 
   *  \param _firstType - data to set
   */
  
  void setFirst(const T & _firstType){ firstType = SharedPointer<T>(new T(_firstType)); }
  
  /*!
   *  \brief Set second type data
   *  \param _secondType - data to set
   */
  
  void setSecond(const U & _secondType){ secondType = SharedPointer<U>(new U(_secondType)); }
};

#endif
