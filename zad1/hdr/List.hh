#ifndef LIST_HH
#define LIST_HH

#include "SmartPointer.hh"
#include <iostream>


template<typename T>
class DLList{
private:
  class Node;

  SharedPointer<Node> first;
  SharedPointer<Node> last;
  int size;
public:
  
  /*!
   *  \brief Constructor of empty doubly linked list
   */
  
  DLList(): first(nullptr), last(nullptr), size(0) {}
  
  /*!
   *  \brief Copy constructor
   *  \param other - list to copy from
   */
  
  DLList(const DLList & other){
    this->clear();
    for(int i = 0; i < other.getSize(); i++)
      this->push_back(other[i]);
  }
  
  /*!
   *  \brief Add new element in the back 
   *  \param _data - data to be added 
   */
   
  void push_back(const T & _data);
  
  /*!
   *  \brief Add new element in the front 
   *  \param _data - data to be added 
   */
  
  void push_front(const T & _data);
  
  /*!
   *  \brief Remove element in the back 
   */
  
  void pop_back();
  
  /*!
   *  \brief Remove element in the front 
   */
  
  void pop_front();
  
  /*!
   *  \brief Add new element after given element 
   *  \param after - index of element 
   *  \param _data - data to be added
   */
  
  void insert(const int & after, const T & _data);
  
  /*!
   *  \brief Remove given element
   *  \param index - index of element to be removed
   */
  
  void remove(const int & index);
  
  /*!
   *  \brief Clear whole list 
   */
  
  void clear();
  
  /*!
   *  \brief Get element in the front 
   *  \return - data in the first node
   */
  
  const T & front() const {
    #ifdef DEBUG
    std::cerr << "first: " << first.get() << std::endl;
    #endif
  
    if(first != nullptr) 
      return first->getData();
    else {
      std::cerr << "empty list" << std::endl; 
      exit(-1);
    }
  }
  
  /*!
   *  \brief Get element in the back
   *  \return - data in the last node
   */
   
  const T & back() const {if(last != nullptr) return last->getData(); else {std::cerr << "empty list" << std::endl; exit(-1);} }
  
  /*!
   *  \brief Get number of elements  
   *  \return - size of the list
   */
  
  const int & getSize() const {return size;}
  
  /*!
   *  \brief Access operator
   *  \param index - index of element to get 
   *  \return - data in given element
   */
  
  const T & operator[](const int & index) const;
  
  /*!
   *  \brief Assignment operator
   *  \param other - list to be copied from
   *  \return - list after assignment
   */
  
  DLList & operator= (const DLList & other){
    this->clear();
    for(int i = 0; i < other.getSize(); i++)
      this->push_back(other[i]);
    return *this;
  }
  
};

template<typename T>
class DLList<T>::Node{
private:
  SharedPointer<T> data;
  SharedPointer<Node> prev;
  SharedPointer<Node> next;
public:
  
  /*!
   *  \brief Constructor of list node
   *  \param _data - data of the new node
   *  \param _prev - pointer to previous node
   *  \param _next - pointer to nest node
   */

  Node(const T & _data, SharedPointer<Node> _prev, SharedPointer<Node> _next) 
            : data(new T(_data)), prev(_prev) ,next(_next) {}
  
  /*!
   *  \brief Set data of current node
   *  \param _data - new data of the node
   */
  
  void setData(const T & _data) {data(new T(_data));}
  
  /*!
   *  \brief Set pointer to previous node
   *  \param _prev - pointer to new node
   */
  
  void setPrev(SharedPointer<Node> _prev) {prev = _prev;}
  
  /*!
   *  \brief Set pointer to next node
   *  \param _next - pointer to new node
   */
  
  void setNext(SharedPointer<Node> _next) {next = _next;}
  
  /*!
   *  \brief Get pointer to previous node
   *  \return - pointer to node
   */
  
  const SharedPointer<Node> & getPrev() const {return prev;}
  
  /*!
   *  \brief Get pointer to next node 
   *  \return - pointer to node
   */
  
  const SharedPointer<Node> & getNext() const {return next;}
  
  /*!
   *  \brief Get data of current node
   *  \return - data of node
   */
  
  const T & getData() const {return *data;}
  
};

template<typename T>
void DLList<T>::push_back(const T & _data){
  SharedPointer<Node> newElem(new Node(_data, last, nullptr));
  
  if(last != nullptr){
    last->setNext(newElem);
  }
  
  last = newElem;
  
  if(first == nullptr){
    first = newElem;
  }
  
  size++;
}

template<typename T>
void DLList<T>::push_front(const T & _data){
  SharedPointer<Node> newElem(new Node(_data, nullptr, first));
  
  if(first != nullptr){
    first->setPrev(newElem);
  }
  
  first = newElem;
  
  if(last == nullptr){
    last = newElem;
  }
  
  size++;
}

template<typename T>
void DLList<T>::pop_back(){
  SharedPointer<Node> tmp;
  if(last != nullptr){
    tmp = last->getPrev();
    last->setPrev(nullptr);
    last = tmp;
  }
  
  if(last != nullptr)
    last->setNext(nullptr);
    
  size--;
}

template<typename T>
void DLList<T>::pop_front(){
  SharedPointer<Node> tmp;
  if(first != nullptr){
    tmp = first->getNext();
    #ifdef DEBUG
    std::cerr << "new first:" << tmp.get() << std::endl;
    #endif
    first->setNext(nullptr);
    first = tmp;
  }
  
  if(first != nullptr)
    first->setPrev(nullptr);
    
  size--;
}

template<typename T>  
void DLList<T>::clear(){
  while(first != nullptr)
    pop_front();
  size = 0;
}

template<typename T>
void DLList<T>::insert(const int & after, const T & _data){
  if(after <= size){
    if(first != nullptr){
      SharedPointer<Node> tmp = first;
      for(int i = 1; i < after; i++){
        tmp = tmp->getNext();
      }
      
      SharedPointer<Node> newElem(new Node(_data, tmp, tmp->getNext()));
      SharedPointer<Node> tmp2 = tmp->getNext();
      
      tmp->setNext(newElem);
      tmp2->setPrev(newElem);
    } else this->push_front(_data);
    size++;
  }
}

template<typename T>
void DLList<T>::remove(const int & index){
  if(index == 0){
    this->pop_front();
  } else if(index == (size - 1)){
    this->pop_back();
  } else if(index < size && index > 0){
    SharedPointer<Node> tmp = first;
    
    for(int i = 0; i < index; i++){
      tmp = tmp->getNext();
    }
    
    SharedPointer<Node> before = tmp->getPrev();
    SharedPointer<Node> after = tmp->getNext();
    
    if(after != nullptr)
      after->setPrev(before);
    
    if(before != nullptr)
      before->setNext(after);
    
    size--;
  }
}
template<typename T>
const T & DLList<T>::operator[](const int & index) const {
  if(index < size && index >= 0){
    SharedPointer<Node> tmp = first;
    for(int i = 0; i < index; i++){
      tmp = tmp->getNext();
    }
  return tmp->getData();
  } else {
    std::cerr << "operator [] out of bounds" << std::endl;  
    exit(-1);
  } 

}

#endif
