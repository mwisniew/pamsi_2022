#include "Board.hh"
#include "Game.hh"
#include <iostream>

#include <chrono>
#include <thread>

int main() {

  Game ttt;
  size_t row;
  size_t col;

  ttt.show_board();

  while(!ttt.is_finished()){

    if(ttt.get_turn() == p_x){
      std::cout << "Row: ";
      std::cin >> row;
      std::cout << "Column: ";
      std::cin >> col;
      if((row - 1)< ttt.game_size() && (col - 1) < ttt.game_size()){
        ttt.move_player(std::make_pair((row - 1), (col - 1)));
      } else {
        std::cerr << "Movement out of bounds!" << std::endl;
        std::chrono::milliseconds u(800);
        std::this_thread::sleep_for(u);
      }

      system("cls");
      ttt.show_board();

    } else {
      ttt.move_bot();

      system("cls");
      ttt.show_board();

    }
  }
  
}

