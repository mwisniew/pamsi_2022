#ifndef ARRAY_HH
#define ARRAY_HH

#include <cstddef>
#include <cstdlib>

template<typename T>
class Array{
  public:
    
    /*! \brief Constructor
     *
     * \param _array - pointer to memory for array
     * \param _size - number of elements to sort
     *
     */
    Array(T* _array, size_t _size) : array(_array), size(_size) {}
    
    /*! \brief Constructor of empty array
     *
     */
    Array() {size = 0;}
    
    /*! \brief Destructor
     *
     */
    ~Array() { if(array != nullptr) delete[] array; }

    /*! \brief Copy constructor
     *
     * \param other - array to make deep copy from
     *
     */
    Array(const Array<T>& other);
    
    
    /*! \brief Assignment operator
     *
     * \param other - array to make deep copy from
     * \return - reference to copy
     *
     */
    Array& operator=(const Array<T>& other);
    
    /*!
     *  \brief Get size
     *  \return - size of array
     */
    const size_t & get_size() const { return size; }
    
    
    /*!
     *  \brief Addition operator
     *  \param other - array to merge 
     *  \return - merged arrays
     */
    Array operator+(const Array<T>& other);
    
    /*!
     *  \brief Addition operator
     *  \param new_el - element to be added to array
     *  \return - array with added element
     */
    Array operator+(const T& new_el);

    /*!
     *  \brief Access operator
     *  \param index - index of element to get 
     *  \return - data in given element
     */
    const T & operator[](size_t index) const {  if(index < size) return array[index]; else exit(-1);}
    
    /*!
     *  \brief Access operator
     *  \param index - index of element to get 
     *  \return - reference to given element
     */
    T & operator[](size_t index) {  if(index < size) return array[index]; else exit(-1);}
    
    /*!
     *  \brief Shrink array
     *  \param new_size - desired size of array
     */
    void shrink(size_t new_size);
    
    /*!
     *  \brief Get array
     *  \return - pointer to array
     */
    T* get_array() {return array;}

  private:
    T* array = nullptr;
    size_t size;
};

template<typename T>
Array<T>::Array(const Array<T>& other) {
  if(this->array != nullptr){
    delete[] this->array;
    this->array = nullptr;
  }
  if(other.array != nullptr){

    this->array = new T[other.size];
    this->size = other.size;

    for(size_t i = 0; i < size; i++){
      this->array[i] = other.array[i];
    }
  }
}

template<typename T>
Array<T>& Array<T>::operator=(const Array<T>& other) {
  if(this->array != nullptr){
    delete[] this->array;
    this->array = nullptr;
  }
  if(other.array != nullptr){

    this->array = new T[other.size];
    this->size = other.size;

    for(size_t i = 0; i < size; i++){
      this->array[i] = other.array[i];
    }
  }
  return *this;
}

template<typename T>
Array<T> Array<T>::operator+(const Array<T>& other){
  if(other.get_size() != 0){

    T* tmp = new T[this->size + other.size];
    for(size_t i = 0; i < this->size; i++)
      tmp[i] = this->array[i];
    for(size_t i = 0; i < other.size; i++)
      tmp[i + this->size] = other.array[i];

    return Array<T>(tmp, this->size + other.size);
  } else return *this;
}

template<typename T>
Array<T> Array<T>::operator+(const T& new_el){
  T* tmp = new T[this->size + 1];
  if(this->size != 0){
    for(size_t i = 0; i < this->size; i++)
      tmp[i] = this->array[i];
    }  
  tmp[this->size] = new_el;

  return Array<T>(tmp, this->size + 1);
}

template<typename T>
void Array<T>::shrink(size_t new_size){
  if(new_size != 0){
    T* tmp = new T[new_size];
  
    for(size_t i = 0; i < size && i < new_size; i++)
      tmp[i] = array[i];
    size = new_size;
    delete[] array;
    array = tmp;
  } else {
    size = 0;
  }
}

#endif // ARRAY_HH
