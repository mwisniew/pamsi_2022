#include "Game.hh"

Game::Game(){
  std::ifstream config_file("config.cfg", std::ifstream::in);
  std::string buf;
  size_t game_size;
  size_t streak;
  size_t minmax_depth;

  while(config_file.peek() == '#')
    std::getline(config_file, buf);

  std::getline(config_file, buf, '=');
  config_file >> game_size;

  while(config_file.peek() == '#')
    std::getline(config_file, buf);

  std::getline(config_file, buf, '=');
  config_file >> streak;

  while(config_file.peek() == '#')
    std::getline(config_file, buf);

  std::getline(config_file, buf, '=');
  config_file >> minmax_depth;

  if(streak == 0)
    streak = game_size;

  game_board = std::make_shared<Board>(game_size, streak);
  ai = std::make_shared<Bot>(minmax_depth);


  current_turn = p_x;
  config_file.close();
}

bool Game::is_finished() const {
  bool is_won = (game_board->won()) ? true : false;
  bool is_full = (game_board->get_moves().size()) ? false : true;

  return (is_won || is_full);
}

void Game::move_player(std::pair<size_t, size_t> location){
  pieces player = (ai->get_symbol() == p_x) ? p_o : p_x;
  if((*game_board)[location.first][location.second] == p_empty){
    (*game_board)[location.first][location.second] = player ;
    current_turn = ai->get_symbol();
  }
}

void Game::move_bot(){
  std::pair<size_t, size_t> location = ai->get_move(*game_board);
  if((*game_board)[location.first][location.second] == p_empty){
    (*game_board)[location.first][location.second] = ai->get_symbol();
    current_turn = (ai->get_symbol() == p_x) ? p_o : p_x;
  }
}

void Game::show_board() const{
  std::cout << std::endl;
  std::cout << std::endl;

  std::cout << *game_board;

  std::cout << std::endl;
  std::cout << std::endl;

}
