#ifndef PACKET_HH
#define PACKET_HH

#include <string>
#include <iostream>

class Packet{
private:
  size_t index;
  std::string name;
  double rating;

public:
  /*! \brief Constructor
   *
   * \param ind - index of movie rating
   * \param nam - movie title
   * \param rat - movie rating
   *
   */
  Packet(size_t ind, std::string nam, double rat) : index(ind), name(nam), rating(rat) {}

  /*! \brief Constructor of empty packet
   *
   *
   */
  Packet(){}

  /*! \brief Operator less than
   *
   * \param other - packet to compare
   * \return - true if rating is lesser than other rating
   *
   */
  bool operator <(const Packet & other) const { return (this->rating < other.rating); }

  /*! \brief Operator greater than
   *
   * \param other - packet to compare
   * \return - true if rating is greater than other rating
   *
   */
  bool operator >(const Packet & other) const { return (this->rating > other.rating); }

  /*! \brief Operator less or equal to
   *
   * \param other - packet to compare
   * \return - true if rating is not greater than other
   *
   */
  bool operator <=(const Packet & other) const { return !(*this > other); }

  /*! \brief Operator greater or equal to
   *
   * \param other - packet to compare
   * \return - true if rating is not lesser than other
   *
   */
  bool operator >=(const Packet & other) const { return !(*this < other); }

  /*! \brief Operator equal to
   *
   * \param other - packet to compare
   * \return - true if index is same as other
   *
   */
  bool operator ==(const Packet & other) const { return (this->index == other.index); }

  /*! \brief Operator not equal to
   *
   * \param other - packet to compare
   * \return - true if index is different than other
   *
   */
  bool operator !=(const Packet & other) const { return !(*this == other); }

  /*! \brief Cast operator
   *
   * \return - movie rating
   *
   */
  operator double() { return rating; }

  /*! \brief Get movie rating
   *
   * \return - movie rating
   *
   */
  const double & get_rating() const { return rating; }

  /*! \brief Get movie title
   *
   * \return - movie title
   *
   */
  const std::string & get_name() const { return name; }

  /*! \brief Get review index
   *
   * \return - review index
   *
   */
  const size_t & get_index() const { return index; }

  /*! \brief Set movie rating
   *
   * \param n_rating - new rating
   *
   */
  void set_rating(const double & n_rating) { rating = n_rating; }

  /*! \brief Set movie title
   *
   * \param n_name - new title
   *
   */
  void set_name(const std::string & n_name) { name = n_name; }

  /*! \brief Set review index
   *
   * \param n_index - new index
   *
   */
  void set_index(const size_t & n_index) { index = n_index; }


};

std::istream & operator >> (std::istream & strm, Packet & packet);
std::ostream & operator << (std::ostream & strm, const Packet & packet);

#endif
