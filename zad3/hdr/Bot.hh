#ifndef BOT_HH
#define BOT_HH

#include <cstddef>
#include <utility>
#include <limits>
#include "Board.hh"

class Bot{
private:
  const int WIN = 100;
  const int LOSS = -100;
  const int DRAW = 0;

  size_t max_depth;
  pieces play_as;


  std::pair<int, std::pair<size_t, size_t>> minimax(Board game_board, const size_t & depth,
                                                    const pieces & current_turn,
                                                    int alpha, int beta);
public:
  Bot(const size_t & depth, const pieces & pcs = p_o) : max_depth(depth), play_as(pcs) {}

  std::pair<size_t, size_t> get_move(Board game_board);

  const pieces & get_symbol() const {return play_as;}
};

#endif // BOT_HH
