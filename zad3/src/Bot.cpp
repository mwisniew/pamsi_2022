#include "Bot.hh"


std::pair<size_t, size_t> Bot::get_move(Board game_board){

  std::pair<size_t, size_t> best_move = minimax(game_board, 0, play_as, LOSS, WIN).second;

  //move (INT_MAX; INT_MAX) means that the algorithm couldn't find leaf node
  if(best_move.first == INT_MAX)
    return game_board.get_moves().at(0);
  else return best_move;
}

std::pair<int, std::pair<size_t, size_t>> Bot::minimax(Board game_board, const size_t & depth, const pieces & current_turn, int alpha, int beta){

  //initializing values
  std::pair<size_t, size_t> best_move = std::make_pair(INT_MAX, INT_MAX);
  int best_value;
  pieces player = (play_as == p_o) ? p_x : p_o;
  pieces winner = game_board.won();

  if(current_turn == play_as)
    best_value = LOSS;
   else best_value = WIN;

  //checking for leaf nodes

  if(game_board.get_moves().empty() || winner != p_empty || depth > max_depth){
    if(winner == p_empty){
      best_value = DRAW;
    } else if(winner == play_as){
      best_value = WIN;
    } else best_value = LOSS;

    return std::make_pair(best_value, best_move);
  }

  std::vector<std::pair<size_t, size_t>> possible_moves = game_board.get_moves();

  for(std::pair<size_t, size_t> move : possible_moves){
    //making temporary move
    game_board[move.first][move.second] = current_turn;

    //maximizing for bot
    if(current_turn == play_as){
      int score = minimax(game_board, depth + 1, player, alpha, beta).first;

      if(best_value < score){
        best_value = score - depth;
        best_move = move;

        alpha = std::max(alpha, best_value);

        game_board[move.first][move.second] = p_empty;

        if(beta <= alpha)
          break;
      }
    } else { //maximizing for player
      int score = minimax(game_board, depth + 1, play_as, alpha, beta).first;

      if(best_value > score){
        best_value = score + depth;
        best_move = move;

        beta = std::min(beta, best_value);

        game_board[move.first][move.second] = p_empty;

        if(beta <= alpha)
          break;
      }
    } //reversing the move
    game_board[move.first][move.second] = p_empty;
  }

  return std::make_pair(best_value, best_move);
}
