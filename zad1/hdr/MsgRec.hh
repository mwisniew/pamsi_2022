#ifndef MSGREC_HH
#define MSGREC_HH

#include <string>
#include "List.hh"
#include "Pair.hh"
#include "Packet.hh"
#include "PriorityQueue.hh"


class MsgRec{
private:
  std::string msg;
public:

  /*!
   *  \brief Constructor of empty message reciver
   */

  MsgRec() {}
  
  /*!
   *  \brief Constructor of message reviver
   *  \param msgSeq - sequence of message packets
   */
  
  MsgRec(const DLList<Pair<DataPacket<std::string>, int>> & msgSeq);
  
  /*!
   *  \brief Get recived message
   *  \return - recived message
   */
  
  const std::string & getMsg() const { return msg; }
};

#endif
