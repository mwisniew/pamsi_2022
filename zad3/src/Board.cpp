#include "Board.hh"

Board::Board(const size_t & _size, const size_t & streak){
  size = _size;
  winning_streak = streak;
  std::vector<pieces> tmp_row;

  for(size_t i = 0; i < size; i++){
    tmp_row.push_back(p_empty);
  }

  for(size_t i = 0; i < size; i++){
    pcs.push_back(tmp_row);
  }
}

bool Board::won_diagonal(const pieces & player) const{
  size_t streak_cntr = 0;

  for(int i = (size - winning_streak); i >= 0; i--){
    streak_cntr = 0;
    for(size_t j = 0; j < (size - i); j++){
      if(pcs[i + j][j] == player)
        streak_cntr++;
      else streak_cntr = 0;

      if(streak_cntr >= winning_streak){
        return true;
      }
    }
  }

  for(size_t j = (size - winning_streak); j > 0; j--){
    streak_cntr = 0;
    for(size_t i = 0; i < (size - j); i++){
      if(pcs[i][i + j] == player)
        streak_cntr++;
      else streak_cntr = 0;

      if(streak_cntr >= winning_streak){
        return true;
      }
    }
  }

  for(size_t i = (size - winning_streak); i < size; i++){
    streak_cntr = 0;
    for(size_t j = 0; j <= i; j++){
      if(pcs[i - j][j] == player)
        streak_cntr++;
      else streak_cntr = 0;

      if(streak_cntr >= winning_streak){
        return true;
      }
    }
  }

  for(size_t i = (size - winning_streak); i > 0; i--){
    streak_cntr = 0;
    for(size_t j = 0; j <= (size - i - 1); j++){
      if(pcs[i + j][size - 1 - j] == player)
        streak_cntr++;
      else streak_cntr = 0;

      if(streak_cntr >= winning_streak){
        return true;
      }
    }
  }

  return false;
}

bool Board::won_horizontal(const pieces & player) const{
  size_t streak_cntr = 0;

  for(size_t i = 0; i < size; i++){
    streak_cntr = 0;
    for(size_t j = 0; j < size; j++){
      if(pcs[i][j] == player)
        streak_cntr++;
      else streak_cntr = 0;

      if(streak_cntr >= winning_streak){
        return true;
      }
    }
  }

  return false;
}

bool Board::won_vertical(const pieces & player) const{
  size_t streak_cntr = 0;

  for(size_t i = 0; i < size; i++){
    streak_cntr = 0;
    for(size_t j = 0; j < size; j++){
      if(pcs[j][i] == player)
        streak_cntr++;
      else streak_cntr = 0;

      if(streak_cntr >= winning_streak){
        return true;
      }
    }
  }

  return false;
}

pieces Board::won() const{
  if(won_vertical(p_x) || won_horizontal(p_x) || won_diagonal(p_x)){
    return p_x;
  }
  if(won_vertical(p_o) || won_horizontal(p_o) || won_diagonal(p_o)){
    return p_o;
  }

  return p_empty;
}

std::vector<std::pair<size_t, size_t>> Board::get_moves(){
  std::vector<std::pair<size_t, size_t>> moves;

  for(size_t i = 0; i < size; i++){
    for(size_t j = 0; j < size; j++){
      if(pcs[i][j] == p_empty)
        moves.push_back(std::make_pair(i, j));
    }
  }
  return moves;
}


std::ostream & operator << (std::ostream & strm, const Board & brd){

  unsigned char l_junction;
  unsigned char i_junction;
  unsigned char r_junction;

  size_t row = 0;
  size_t col = 0;

  size_t board_lines = (2 * brd.get_size() + 1);

  for(size_t i = 0; i < board_lines; i++){

    if(i == 0){

      l_junction = edges::left_upper_corner;
      i_junction = edges::top_t_junction;
      r_junction = edges::right_upper_corner;

    } else if(i == (board_lines - 1)){

      l_junction = edges::left_bottom_corner;
      i_junction = edges::bot_t_junction;
      r_junction = edges::right_bottom_corner;

    } else {

      l_junction = edges::left_t_junction;
      i_junction = edges::junction;
      r_junction = edges::right_t_junction;

    }

    if((i % 2) == 0){
      strm << l_junction;
    } else {
      strm << edges::vertical_line;
    }

    for(size_t j = 0, col = 0; j < brd.get_size(); j++){

      if((i % 2) == 0){
        strm << edges::horizontal_line
             << edges::horizontal_line
             << edges::horizontal_line;

        if(j != (brd.get_size() - 1)){
          strm << i_junction;
        } else {
          strm << r_junction;
        }

      } else {

        switch(brd[row][col++]){
          case p_empty:
            strm << "   ";
            break;

          case p_x:
            strm << " X ";
            break;

          case p_o:
            strm << " O ";
            break;

        }
        strm << edges::vertical_line;
      }

    }

    if((i % 2) != 0)
      row++;

    strm << std::endl;

  }
  return strm;
}
