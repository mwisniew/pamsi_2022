#include "MsgRec.hh"

MsgRec::MsgRec(const DLList<Pair<DataPacket<std::string>, int>> & msgSeq){
  PriorityQueue<DataPacket<std::string>, int> msgQueue(msgSeq);

  std::string buf;

  while(msgQueue.getSize()){
    buf += msgQueue.front().getFirst().getData();
    msgQueue.pop_front();
  }
  msg = buf;
}
